/**Bài 1: Tìm số nguyên dương nhỏ nhất sao cho:
 * 1+2+3+...+n>10000
 */
onload = function findMinInt(){
    // input
    var x = 1e+4;
    // output
    var sum = 0;
    // process
    for(var i = 1; i<x;i++){
        sum += i;
        if(sum>x){
            break;
        }
        else{
            continue;
        }
    }
    document.querySelector("#ketQua1").innerHTML = i;
}
// var x = 2*10000;
// var result = 0;
// var delta = 1 + 4*x;
// var x1 = (-1 + Math.sqrt(delta))/2;
// var x2 = (-1 - Math.sqrt(delta))/2;
// if(x1>0){
//     result = Math.ceil(x1);
// } else {
//     result = Math.ceil(x2);
// }
// console.log(result);

/**Bài 2: Nhập vào 2 số x,n tính tổng: */
document.querySelector("#btnPart2").onclick = function() {
    // input
    var soX = document.querySelector("#soX").value*1;
    var soN = document.querySelector("#soN").value*1;
    // output
    var sum = 0;
    // process
    for(var i = 1;i<=soN;i++)
    {
        sum += Math.pow(soX,i);
    }
    document.querySelector("#ketQua2").innerHTML = sum;
}
/**Bài 3: Tính giai thừa */
document.querySelector("#btnPart3").onclick = function() {
    // input 
    var soGiaiThua = document.querySelector("#soGiaiThua").value*1;
    // output
    var giaiThua = 1;
    // process
    for(var i = 1; i<=soGiaiThua; i++)
    {
        giaiThua *= i;
    }
    document.querySelector("#ketQua3").innerHTML = giaiThua;
}
/**Bài 4: In ra thẻ div */

document.querySelector("#btnPart4").onclick = function() {
    // input
    var soTheDiv = 10;
    // output
    var result = '';
    // process
    for(var i=1; i<=10; i++)
    {
        if(i%2!=0){
            result += `<div class="offset-sm-4 col-sm-4 alert alert-danger">Div lẻ ${i}</div>`
        } else {
            result += `<div class="offset-sm-4 col-sm-4 alert alert-primary">Div chẵn ${i}</div>`
        }
    }
    document.querySelector("#inTheDiv").innerHTML = result;
}
